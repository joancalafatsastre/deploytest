const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
var optimizarimagenes = require('gulp-imagemin');

gulp.task('default', defaultTask);

function defaultTask(done) {
        optimizarImagenes();
        autoprefixerCSS();
        done();
}


function optimizarImagenes() {
        gulp.src('web/img/*')
        .pipe(optimizarimagenes())
        .pipe(gulp.dest('web/img'))
}

function autoprefixerCSS(){
        gulp.src('web/css/*.css')
                .pipe(autoprefixer({
                        browsers: ['last 2 versions'],
                        cascade: false
                }))
                .pipe(gulp.dest('web/css'))


}

